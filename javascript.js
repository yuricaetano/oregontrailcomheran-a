class Traveler {
    constructor(name) {
        this.name = name;
        this.food = 1;
        this.isHealthy = true;
    }
    hunt() {
        this.food += 2;
    }
    eat() {
        if (this.food == 0) {
            this.isHealthy = false;
        } else {
            this.food -= 1;
        }
    }
}

class Doctor extends Traveler {
    constructor(name){
        super(name)
        this.name = name;
        this.food = 1;
        this.isHealthy = true;
    }
    heal(viajante){
        viajante.isHealthy = true;
    }
}

class Hunter extends Traveler {
    constructor(name){
        super(name)
        this.name = name;
        this.food = 2;
        this.isHealthy = true;
    }
    hunt(){
        this.food += 5;
    }
    eat(){
        if (this.food > 2){
            this.food -= 2;
        }else{
            this.food = 0;
            this.isHealthy = false;
        }
    }
    giveFood(traveler, numOfFoodUnits){
        if (this.food <= numOfFoodUnits){
        }else{
            traveler.food = numOfFoodUnits;
            this.food -= numOfFoodUnits;
        }
    }
}


class Wagon {
    constructor(capacity) {
        this.capacity = capacity;
        this.passageiros = [];
        
    }    
    getAvailableSeatCount(){
        this.vagas = this.capacity - this.passageiros.length;
        return this.vagas;
    }
    join(passenger){
        let passageiro = passenger;
        if (this.getAvailableSeatCount() > 0) {
            this.passageiros.push(passageiro)
        }
    }
    shouldQuarantine(){
        for (let i = 0; i < this.passageiros.length; i++) {
            if (this.passageiros[i].isHealthy === false) {
                return true;
            }
        }
        return false;
    }
    totalFood(){
        let comidaTotal = 0;
        for (let y = 0; y < this.passageiros.length; y++) {
            comidaTotal += this.passageiros[y].food;
        }
        return comidaTotal;
    }
}

// Create wagon with 4 slots 
let wagon = new Wagon(4);
// Create 5 travelers
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let drsmith = new Doctor('Dr. Smith');
let sarahunter = new Hunter('Sara');
let maude = new Traveler('Maude');

console.log(`#1: There should be 4 available seats. Actual: ${wagon.getAvailableSeatCount()}`);

wagon.join(henrietta);
console.log(`#2: There should be 3 available seats. Actual: ${wagon.getAvailableSeatCount()}`);

wagon.join(juan);
wagon.join(drsmith);
wagon.join(sarahunter);

wagon.join(maude); // Dont have space to she!
console.log(`#3: There should be 0 available seats. Actual: ${wagon.getAvailableSeatCount()}`);

console.log(`#4: There should be 5 total food. Actual: ${wagon.totalFood()}`);

sarahunter.hunt(); // Take more 5 foods
drsmith.hunt();

console.log(`#5: There should be 12 total food. Actual: ${wagon.totalFood()}`);

henrietta.eat();
sarahunter.eat();
drsmith.eat();
juan.eat();
juan.eat(); // Juan now is sick

console.log(`#6: Quarantine should be true. Actual: ${wagon.shouldQuarantine()}`);
console.log(`#7: There should be 7 total food. Actual: ${wagon.totalFood()}`);

drsmith.heal(juan);
console.log(`#8: Quarantine should be false. Actual: ${wagon.shouldQuarantine()}`);

sarahunter.giveFood(juan, 4);
sarahunter.eat(); // She only have one, so she's eat and be sick


console.log(`#9: Quarantine should be true. Actual: ${wagon.shouldQuarantine()}`);
console.log(`#10: There should be 6 total food. Actual: ${wagon.totalFood()}`);